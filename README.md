> **Preamble:** We strive to make marine image data [FAIR](https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/metadata-profiles-fdos/-/blob/master/FAIR-marine-images.md). We maintain [data profiles](https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/metadata-profiles-fdos) to establish a common language for marine imagery, we develop best-practice [operating procedures](https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/standard-operating-procedures) for handling marine images and - here in this repository - we develop [software tools](https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/software/mar-iqt) to apply the vocabulary and procedures to marine imagery.

# MarIQT Curation Notebooks

This folder contains user-friendly notebooks to run (image) data curation processes. The notebooks are based on the MarIQT [python package](https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/software/mar-iqt/-/tree/master/mariqt) which may be helpful to you on its own. Both the python package and the notebooks were conceptualised and developed by the MareHub working group on Videos/Images (part of the DataHub, a research data management initiative by the Helmholtz association).

## Run Jupyter Notebooks

- install Python
- its recommend to use a virtual environment
  - create virtual environment (name it e.g. ".env") in current directory: `python -m venv .env`
  - activate it - unix: `source .env/bin/activate`, windows: `.env/Scripts/activate`
- install dependencies
  -  ```pip install -r requirements.txt```
  - you might need to install exempi (```apt-get install exempi```)
  - if using virtualenv on Windows run ```ipython kernel install --user --name=venv``` in activated venv and select kernel via notebook web gui
- *curate_navigationData*, *AddUUIDsToFiles*, *iFDO_create* are optimized for the use with [Appmode](https://github.com/oschuett/appmode). Activate with: 
  - ```jupyter nbclassic-extension enable --py --sys-prefix appmode``` (if you are not using a virtualenv omit ``` --sys-prefix ``` )
  - ```jupyter server    extension enable --py --sys-prefix appmode``` (if you are not using a virtualenv omit ``` --sys-prefix ``` )
- run Jupyter Notebook
  - ```jupyter notebook``` (if jupyter not found it may help to log out and in again)
- open notebook and potentially click on *Appmode*
  - if there is no *Appmode* button you may need to click on *Open in...* and select "NbClassic"
  - if upon clicking *Appmode* you get an error **400 : Bad Request, Cannot create file...** start notebook with ```--ContentsManager.allow_hidden=True```

# The notebooks

## Process overview

### 1_CurationOverview

### 1_CurationOverviewImages

## Navigation data curation

### Navigation1-queryDSHIPForNavigationData
*Extracts action information from the DSHIP action log and queries the BSH for USBL or GPS navigation data:*
This script needs an action log export file and information on USBL beacons used per gear. Then it extracts start and end times from the action log as an input for the DSHIP USBL / GPS data extraction and executes the DSHIP extraction through the BSH DSHIP REST "API". GPS data is requested (i.e. navigation data of the vessel) in case no USBL data is expected for a specific gear type.

### Navigation2_downloadDSHIPZips
### Downloads navigation data from a DSHIP instance
*This script queries a DSHIP instance for navigation data.*
This is only useful in case you created a request there beforehand with the queryDSHIPForNavigationData notebook.

### Navigation3-processDSHIPNavigationData
*This script batch-processes exported DSHIP Navigation *.zip files (USBL and GPS)*
All downloaded zip files from the DSHIP export (see Curation_Navigation1-...) are moved to the appropriate locations by this script, and then quality controlled and processed into publication-ready data products.

### Navigation4-collateNavigationForCruise

### curate_navigationData

This notebook allows you to parse navigation data from different sources, merge and plot them, run basic filtering on the data end export the data to a single navigation file.

## Image data curation

### AddUUIDsToFiles

This notebook allows you to write UUIDs to image files them self as required for creating iFDOs.

### iFDO_create

This notebook walks you through the steps of creating an iFDO file for your set of images.

### iFDO_exportToExcel

Use this notebook to export a set of header fields from iFDO file(s) to an Excel sheet so that the values can be conveniently examined and potentially edited.

### iFDO_updateFromExcel

Use this notebook to updates iFDO's header fields from an Excel sheet.

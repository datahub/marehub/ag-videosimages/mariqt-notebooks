{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Tested with mariqt version 1.0.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Creating iFDOs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import mariqt.directories as miqtd\n",
    "import mariqt.sources.ifdo as miqtifdo"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup Image Directory\n",
    "\n",
    "An iFDO object needs to be initialized with a directory object containing to a folder called *raw* (see [folder structure](https://marine-imaging.com/fair/sops/conventions/folder-structure/)) which contains your raw images.\n",
    "\n",
    "**The image files need to contain UUIDs (v4)**\n",
    "\n",
    "If they don't, see [Adding UUIDs to Image Files](#add-uuids-to-image-files).\n",
    "\n",
    "**The image files need to comply with the file naming convention**: `<event>_<sensor>_<date>_<time>.<ext>`\n",
    "\n",
    "where\n",
    "  * the *event* format must be `<project>[-<project part>]_<event-id>[-<event-id-index>][_<device acronym>]`, e.g.: SO268_012-1\n",
    "  * the *sensor* format must be `<owner>_<type>-<type index[_<subtype>[_<name>]]>`, e.g. GMR_CAM-01\n",
    "  * the *date and time* (in **UTC**) formats must be `yyyymmdd` and `HHMMSS[.zzz]`, respectively\n",
    "  * [..] is optional\n",
    "\n",
    "In case your files do not comply with this convention, you can use the [Tomato](https://git.geomar.de/dsm/image-workflow/tomato-toolboxes/tomato-tools-info) Tool [FileRename](https://git.geomar.de/dsm/image-workflow/tomato-toolboxes/filerename) to rename you files accordingly.\n",
    "\n",
    "If there is already an iFDO file in the *products* folder, this file will be loaded."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "images_dir = \"...\" # path to your raw images\n",
    "miqt_dir = miqtd.Dir(\"\",images_dir, create=False, with_gear=False)\n",
    "ifdo = miqtifdo.iFDO(miqt_dir)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Alternatively**, an iFDO object can be created from an explicit iFDO-file or, for header-only editing, without having to have the actual image files at hand."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ifdo_file = \"...\"\n",
    "ifdo = miqtifdo.ifdoFromFile(ifdo_file, ignore_image_files=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "New field values are first written to a temporary, unchecked record and can be checked and set to the actual iFDO later on.\n",
    "\n",
    "You can always check the current unchecked and checked ifdo:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ifdo.getUnchecked()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ifdo.getChecked()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Update/Provide Metadata\n",
    "\n",
    "An iFDO consists of two parts: \n",
    "* a) the *Header* - containing information for the while data set\n",
    "* b) the *Items* - containing individual information per image item. \n",
    "\n",
    "### a) Header Information "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "header_fields = {\n",
    "    'image-abstract': \"500 - 2000 characters describing what, when, where, why and how the data was collected. Includes general information on the event (aka station, experiment), e.g. overlap between images/frames, parameters on platform movement, aims, purpose of image capture etc.\",\n",
    "    #'image-altitude-meters': \"\", # if empty will be automatically set representatively for respective item values \n",
    "    'image-context': {'name': \"My cool project\"},\n",
    "    'image-coordinate-reference-system': \"EPSG:4326\",\n",
    "    'image-coordinate-uncertainty-meters': 2,\n",
    "    'image-copyright': \"My copyright statement\",\n",
    "    'image-creators': [{'name': \"Jane Doe\", 'uri': \"https://orcid.org/0000-0002-1825-0097\"},\n",
    "                       {'name': \"John Doe\", 'uri': \"https://orcid.org/0000-0002-1825-0097\"}],\n",
    "    #'image-datetime': \"\", # if empty will be automatically set representatively for respective item values \n",
    "    'image-event': {'name': \"POS123_003_AUV-01\"},\n",
    "    #'image-latitude': \"\", # if empty will be automatically set representatively for respective item values \n",
    "    'image-license': {'name': \"CC-0\"},\n",
    "    #'image-longitude': \"\", # if empty will be automatically set representatively for respective item values \n",
    "    'image-pi': {'name': \"Jane Doe\", 'uri': \"https://orcid.org/0000-0002-1825-0097\"},\n",
    "    'image-platform': {'name': \"GMR_PFM-126_AUV_Sparus-II-ALBERT\"},\n",
    "    'image-project': {'name': \"POS123\"},\n",
    "    'image-sensor': {'name': \"GMR_CAM-1\"},\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The information ca be used to *update* the (unchecked) iFDO record: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ifdo.updateHeaderFields(header_fields)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Or** to *overwrite* it: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ifdo.setHeaderFields(header_fields)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some fields can be auto-filled from others or external sources:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ifdo.setImageSetNameFieldFromProjectEventSensor()\n",
    "ifdo.trySetHeaderImageProjectUriToOsisExpeditionUrl()\n",
    "ifdo.trySetHeaderImageEventUriToOsisEventUrl()\n",
    "ifdo.trySetHeaderEquipmentUriToHandleUrl('image-sensor')\n",
    "ifdo.trySetHeaderEquipmentUriToHandleUrl('image-platform')\n",
    "ifdo.trySetLicenseUriFromLicenseName()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### b) Items Information\n",
    "\n",
    "For items information its convenient to compile the information in intermediate text table files first. \n",
    "\n",
    "#### Creating Intermediate Files\n",
    "\n",
    "##### Start Times, UUIDs, Hashes (Required)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "msg = ifdo.createStartTimeFile()\n",
    "msg = ifdo.createUuidFile()\n",
    "msg = ifdo.createImageSha256File()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Navigation (Required)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "navigation_file = \"...\" # path to your navigation data\n",
    "nav_header = {  'utc': 'DateTime', \n",
    "                'lat': 'Latitude', \n",
    "                'lon': 'Longitude', \n",
    "                'dep': 'Depth',\n",
    "                'hgt': 'Altitude', # optional\n",
    "                #'uncert', 'Coordinate uncertainty', # optional\n",
    "                'yaw': 'Yaw', 'pitch': 'Pitch', 'roll': 'Roll' # only needed if lever arm compensation is to be done\n",
    "                }\n",
    "date_format = \"%Y-%m-%d %H:%M:%S.%f\"\n",
    "offset_x,offset_y,offset_z = 0,0,0 # optional, lever arm offsets in meters in vehicle coordinates\n",
    "video_sample_seconds = 1 # in case of video files every nth second navigation data is written\n",
    "msg = ifdo.createImageNavigationFile(   navigation_file,\n",
    "                                        nav_header=nav_header,\n",
    "                                        date_format=date_format,\n",
    "                                        overwrite=True,\n",
    "                                        col_separator=\",\",\n",
    "                                        video_sample_seconds=video_sample_seconds,\n",
    "                                        offset_x=offset_x,offset_y=offset_y,offset_z=offset_z,angles_in_rad=False) # optional"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Attitude (Optional)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Constant values for whole set:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "yaw_frame, pitch_frame, roll_frame = 0,0,0\n",
    "yaw_cam2frame, pitch_cam2frame, roll_cam2frame = 0,0,0\n",
    "ifdo.setImageSetAttitude(   yaw_frame,\n",
    "                            pitch_frame,\n",
    "                            roll_frame,\n",
    "                            yaw_cam2frame,\n",
    "                            pitch_cam2frame,\n",
    "                            roll_cam2frame)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Time series values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "frame_att_header = {'utc':   'DateTime', \n",
    "                    'yaw':   'Yaw', \n",
    "                    'pitch': 'Pitch', \n",
    "                    'roll':  'Roll'}\n",
    "msg = ifdo.createImageAttitudeFile( navigation_file, \n",
    "                                    frame_att_header, \n",
    "                                    yaw_cam2frame, yaw_cam2frame, yaw_cam2frame,\n",
    "                                    date_format, \n",
    "                                    overwrite=True, \n",
    "                                    col_separator=\",\",\n",
    "                                    att_path_angles_in_rad=False,\n",
    "                                    video_sample_seconds = video_sample_seconds)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Further Items Fields (Optional)\n",
    "\n",
    "For other item fields you can provide your own intermediate files to be loaded. They need to contain the file name (and time for videos timestamps) and the corresponding iFDO field value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "item_data_file = \"...\" # path to your item data file\n",
    "header = {'image-filename':'image-filename', 'image-entropy':'entropy'}\n",
    "ifdo.addItemInfoTabFile(item_data_file, separator=\",\", header=header)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Exif data per image can be extracted and added at once with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "msg = ifdo.createAcquisitionSettingsExifFile()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Check Provided Information and Create/Update Fields\n",
    "\n",
    "So far the information has been written to the unchecked iFDO record or to intermediate files only. Now you need to create/update the actual iFDO. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Update**\n",
    "\n",
    "The newly provided header information and the loaded item data from the intermediate files is validated. If everything is fine the previously checked iFDO record (if any yet) is being updated with the new information. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ifdo.updateFields()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Create** \n",
    "\n",
    "Same as *update* except that a previously checked iFDO record is not updated but **overwritten**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ifdo.createFields()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Write iFDO File"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ifdo.writeIfdoFile()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Reading iFDOs\n",
    "\n",
    "To read specific data from a iFDO object you use e.g. the following. If an item/item-timestamp does not contain a field but a superior field (header/item-default) does, the latter is returned."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Reading from checked iFDO record:\n",
    "# raise exception if field not found\n",
    "ifdo['POS123_003_AUV-01_GMR_CAM-1_20240320_111115.JPG:image-altitude-meters']\n",
    "ifdo['POS123_003_AUV-01_GMR_CAM-1_20240320_111115.JPG:image-event']\n",
    "ifdo.getCheckedValue('POS123_003_AUV-01_GMR_CAM-1_20240320_111115.JPG:image-event')\n",
    "# returns empty string if field not found\n",
    "ifdo.findCheckedValue('POS123_003_AUV-01_GMR_CAM-1_20240320_111115.JPG:image-event')\n",
    "\n",
    "# Reading from unchecked iFDO record:\n",
    "# raise exception if field not found\n",
    "ifdo.getUncheckedValue('POS123_003_AUV-01_GMR_CAM-1_20240320_111115.JPG:image-event')\n",
    "# returns empty string if field not found\n",
    "ifdo.findUncheckedValue('POS123_003_AUV-01_GMR_CAM-1_20240320_111115.JPG:image-event')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, you can use the `IfdoReader` to simply read from an iFDO file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ifdo_file = \"...\"\n",
    "ifdo_reader = miqtifdo.IfdoReader(ifdo_file)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# raise exception if field not found\n",
    "ifdo_reader['POS123_003_AUV-01_GMR_CAM-1_20240320_111115.JPG:image-altitude-meters']\n",
    "# returns empty string if field not found\n",
    "ifdo_reader.find('image-altitude-meters')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": ".env",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
